from django.urls import path
from .views import (
    RecipeListView, 
    AccountCreateView, 
    CategoriesCreateView, 
    ReceiptCreateView,
    AccountListView, 
    CategoryListView,
)


urlpatterns = [
    path("", RecipeListView.as_view(), name="home"),
    path("accounts/create", AccountCreateView.as_view(), name="account_create"),
    path("categories/create", CategoriesCreateView.as_view(), name="category_create"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path("accounts/", AccountListView.as_view(), name="list_accounts"),
    path("categories/", CategoryListView.as_view(), name="categories_list"),    
]
